# Hahow Backend Engineer 徵才小專案

這是一個小型API專案, 使用Express.js + ES6撰寫, 所以開啟production server前記得先跑build

## 專案架構

### 主架構
|檔案|說明|
|---|---|
|bin|執行檔相關|
|src|源碼資料夾|
|lib|編譯後的資料夾|
|apidoc|API文件|

### src架構
src/內包含了以下資料夾, 分別用來放不同用途的程式

|檔案|說明|
|---|---|
|app.js|Express app主要進入點|
|api|每一個資料夾就是一個API Resource, 資料夾下的index定義route, controller為整理流程和資料的地方|
|partner|第三方api的wrapper|
|utils|其他輔助相關|

> 1. 每新增一個資料夾在api底下就是一個新的resource, 之後可以做成cli
> 2. 專案有export出來的function是考量到以後方便寫測試


## 安裝
可以直接下載壓縮檔或從bitbucket clone下來並且安裝

```sh
$ git clone https://bitbucket.org/tccchong/tcc-hahow-hw.git
$ npm install
```

## 啟動測試Server
測試server會根據改的檔案即時把ES6的code編譯為ES5(做法參考[parse-server](https://github.com/ParsePlatform/parse-server/blob/master/bin/dev))

```sh
$ npm run dev
```

## 從ES6轉到ES5
deploy到正式環境的話記得先build code再開啟

```sh
$ npm run build
```

## 啟動正式Server
開啟前請先確保code build好了(可以用git-hooks輔助自動build code)

```sh
# 一般node
$ npm run prod

# 使用pm2(如果環境有裝的話, 沒有的話請先npm i pm2 -g)
$ npm run prod-pm2
```

## curl測試
bin的資料夾有個叫做curl的檔案, 可以用來跑全部routes的測試, 需要改參數可以直接改
(一般正式測試會用mocha + chai, 但是在這個專案就用簡單的curl來取代了)

```sh
# 跑curl測試
$ npm run test
```

## API文件產生

運行以下指令後會產生一個apidoc的資料夾, 裏面就有相對應的文件檔

```sh
$ npm run gen-apidoc
```


## 第三方Library
* [babel](http://babeljs.io) - 使用ES6寫程式
* [express](https://github.com/expressjs/express/?_ga=1.221434840.1517927487.1386211128) - Web框架
* [superagent](https://github.com/visionmedia/superagent) - Http request library
* [gaze](https://github.com/shama/gaze) - 監控檔案變化並針對檔案做事情, 用來配合babel開發時即時轉換ES6至ES5
* [nodemon](https://github.com/remy/nodemon) - 監控檔案變化並重啓Service
* [apidoc](https://github.com/apidoc/apidoc) - REST API文件產生器

## 其他
> 你在程式碼中寫註解的原則，遇到什麼狀況會寫註解

一般在以下情況會寫註解:

1. 流程控制寫清楚流程會如何進行
2. 標明檔案或function的用途是什麼，會傳什麼參數，參數型態是什麼
3. API文件相關

> 在這份專案中你遇到的困難、問題，以及解決的方法

最大問題在於如何用一份簡單的文件表達出這個專案的架構，只能參考別人的再自己琢磨琢磨了。
