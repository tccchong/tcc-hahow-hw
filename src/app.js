import express from 'express';
import configureMiddlewares from './utils/configureMiddlewares';
import configureRoutes from './utils/configureRoutes';
import { errorHandler } from './utils/middlewares';

const app = express();
const PORT = process.env.API_PORT || 3333;

configureMiddlewares(app);
configureRoutes(app);

app.use(errorHandler);
app.listen(PORT);
console.log(`Server listening ${PORT}`);
