import ApiError from './ApiError';
import { auth } from '../partner/HahowAPI';

/**
 * Middleware function for authentication, get user auth info from headers
 * @param {String} name User name
 * @param {String} password User password
 */
export function authentication(req, res, next) {
  const {
    name,
    password
  } = req.headers;

  if (!name && !password) {
    return next();
  } else if ((name && !password) || (!name && password)) {
    return next(new ApiError(ApiError.MISSING_PARAMS, 'Authentication failed'));
  } else {
    auth({
      name,
      password
    })
    .then(() => {
      req.hasAuth = true;
      return next();
    })
    .catch((error) => {
      return next(error);
    });
  }
}

/**
 * Middleware function for error handle
 */
export function errorHandler(error, req, res, next) {
  if (error instanceof ApiError) {
    const {
      code,
      message,
      status
    } = error;
    res.status(status || 500).json({ code, message });
  } else {
    console.log('Unknown error:', error);
    res.sendStatus(500);
  }
}
