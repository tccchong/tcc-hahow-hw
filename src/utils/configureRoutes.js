import fs from 'fs';
import path from 'path';

function getRoutes() {
  return fs.readdirSync(path.resolve(__dirname, '../api/'));
}

/**
 * Helper function for express router configuration
 * @param {ExpressApp} app Express instance
 */
export default function configureRoutes(app) {
  const routes = getRoutes();

  // Find all dir from api/
  for (let file of routes) {
    const route = `/${file}`;
    const routeConst = require('../api/' + route);
    
    app.use(route, routeConst);
  }
}
