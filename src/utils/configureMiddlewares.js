import { authentication } from './middlewares';

/**
 * Helper function for middleware configuration
 * @param {ExpressApp} app Express instance
 */
export default function configureMiddlewares(app) {
  app.use(authentication);
}
