import request from 'superagent';
import ApiError from './ApiError';

/**
 * Helper function for sending http request
 * @param {String} endpoint The endpoint of request
 * @param {String} method Http request method
 * @param {Object} options 
                    - {Object} data Request data when using http post
 */
export function sendRequest(endpoint, method = 'get', options = {}) {
  return new Promise((resolve, reject) => {
    const responseHandler = (error, response) => {
      if (error) {
        let apiError;
        switch (response.statusCode) {
          case 400:
            apiError = new ApiError(ApiError.BAD_REQUEST, 'You are sending a bad request', 400);
            break;
          case 401:
            apiError = new ApiError(ApiError.UNAUTHRORIZED, 'You are sending an unauthrozied request', 401);
            break;
          case 404:
            apiError = new ApiError(ApiError.NOT_FOUND, 'Request url not found', 404);
            break;
          default:
            apiError = new ApiError(ApiError.UNEXPECTED, 'Api request unexpected error occurs');
            break;
        }
        reject(apiError);
      } else {
        if (response.statusCode === 200) {
          resolve(response.body);
        } else {
          reject(new ApiError(ApiError.UNEXPECTED, 'Api request unexpected error occurs'));
        }
      }
    };

    const data = options.data || {};

    let apiRequest = request[method](endpoint);
    apiRequest.set('accept', 'application/json');
    apiRequest.set('content-type', 'application/json');

    apiRequest.send(data);
    apiRequest.end(responseHandler);
  });
}
