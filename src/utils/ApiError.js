/**
 * Constructs a new object with given code and message
 * @constructor
 * @param {Number} code Error code constant
 * @param {String} message Description of the error
 */
export default class ApiError {
  constructor(code, message, status) {
    this.code = code;
    this.message = message;
    this.status = status;
  }
}

ApiError.MISSING_PARAMS = 100;
ApiError.BAD_REQUEST = 400;
ApiError.UNAUTHRORIZED = 401;
ApiError.NOT_FOUND = 404;
ApiError.UNEXPECTED = 999;
