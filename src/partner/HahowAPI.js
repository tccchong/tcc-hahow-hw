/**
 * Helper for sending api request to Hahow
 */
import { sendRequest } from '../utils/apiClient';

const HOST = 'http://hahow-recruit.herokuapp.com';

// GET /heroes
export function listHeroes() {
  return callApi('/heroes');
}

// GET /heroes/:id
// @param {Number} id
export function getHeroes(id) {
  return callApi(`/heroes/${id}`);
}

// GET /heroes/:id/profile
// @param {Number} id
export function getHeroesProfile(id) {
  return callApi(`/heroes/${id}/profile`);
}

// POST /auth
// @param {Object} data
//            - {String} name
//            - {String} password
export function auth(data) {
  return callApi(`/auth`, 'post', { data });
}

// Wrapper function to call api
function callApi(path, method, options) {
  return Promise
          .resolve(sendRequest(`${HOST}${path}`, method, options))
          .catch(handleApiError);
}

function handleApiError(error) {
  throw error;
}
