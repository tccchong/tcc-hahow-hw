import { Router as expressRouter } from 'express';
import { handleList, handleGet } from './controller';

const Heroes = expressRouter();
/**
 * @apiVersion 1.0.0
 * @api {get} /heroes
 * @apiName ListHero
 * @apiGroup heroes
 *
 * @apiSuccess {String} id An unique id of hero
 * @apiSuccess {String} name Name of hero
 * @apiSuccess {String} image Profile pic of hero
 * @apiSuccess {Object} profile Profile information
 * @apiSuccess {Number} profile.str Strength
 * @apiSuccess {Number} profile.int Intelligent
 * @apiSuccess {Number} profile.agi Agility
 * @apiSuccess {Number} profile.luk Lucky
 */
Heroes.route('/').get(handleList);

/**
 * @apiVersion 1.0.0
 * @api {get} /heroes/:heroId
 * @apiName GetHero
 * @apiGroup heroes
 *
 * @apiParam {String} heroId An unique id of hero
 *
 * @apiSuccess {String} id An unique id of hero
 * @apiSuccess {String} name Name of hero
 * @apiSuccess {String} image Profile pic of hero
 * @apiSuccess {Object} profile Profile information
 * @apiSuccess {Number} profile.str Strength
 * @apiSuccess {Number} profile.int Intelligent
 * @apiSuccess {Number} profile.agi Agility
 * @apiSuccess {Number} profile.luk Lucky
 */
Heroes.route('/:heroId').get(handleGet);

module.exports = Heroes;
