import apiClient from '../../utils/apiClient';
import * as HahowAPI from '../../partner/HahowAPI';

export function handleList(req, res, next) {

  /*
   1. Retrieve hero list
   2. If request.hasAuth is true then goto extend profile, else send response with simple list
   3. Extend profile to hero list and send response
  */

  // #1
  HahowAPI.listHeroes().then(responseOrExtendProfile).catch((error) => {
    return next(error);
  });

  // #2
  function responseOrExtendProfile(list) {
    if (req.hasAuth) {
      // Transform to promise list to call api by id
      const promises = list.map((obj) => {
        return HahowAPI.getHeroesProfile(obj.id);
      });
      return Promise.all(promises).then(extendAndResponse(list));
    } else {
      res.status(200).json(list);  
    }
  }

  // #3
  function extendAndResponse(userList) {
    return (profileList) => {
      // Extend profile to hero
      const listWithProfile = userList.map((obj, index) => {
        return Object.assign(obj, { profile: profileList[index] });
      });
      res.status(200).json(listWithProfile);
    }
  }
}

export function handleGet(req, res, next) {

  /*
   1. Retrieve hero data by id
   2. If request.hasAuth is true, add get profile promise to retrieve profile
   3. Return user info with profile if exists, otherwise return hero base info
  */

  let promises = [];
  // #1 
  promises.push(HahowAPI.getHeroes(req.params.heroId));

  // #2
  if (req.hasAuth) {
    promises.push(HahowAPI.getHeroesProfile(req.params.heroId));
  }

  Promise.all(promises).then(response).catch((error) => {
    return next(error);
  });

  // #3
  function response(results) {
    const userInfo = results[0];
    const profile = results[1] ? { profile: results[1] } : {};

    return res.status(200).json(Object.assign(userInfo, profile));
  } 
}
